import os
import shutil

import invoke
import packaging.version


# TODO: Use a standard gitlab PyPI CI to push releases on tag pushes
@invoke.task
def release(ctx, test=True):
    """Pushes the package to pypi

    Steps involved:
    1. Prevent pushing to official PyPI if not in main branch
    2. Pull the latest changes from the remote
    3. Check that the current commit has been tagged
    4. Runs the tests
    5. Make the docs
    6. Builds the package and checks if it is valid
    7. Uploads the package to pypi
    8. Push the tag to remote
    """
    # branches (appart from main) can only be published on TestPyPI
    branch = ctx.run("git branch --show-current", hide=True).stdout.strip()
    if branch != "main" and not test:
        raise invoke.Exit(
            "Not in main branch. Aborting release.\n\
            either switch to main branch or \n\
            push to test pypi using `--test=False` flag "
        )

    ctx.run("git pull")

    version = packaging.version.parse(
        ctx.run("python3 -m setuptools_scm").stdout.strip()
    )
    if version.is_devrelease:
        raise invoke.Exit(
            "Current commit is a development commit.\n"
            "Please tag the current release appropriately and re-run the script."
        )

    ctx.run("pytest")
    ctx.run("cd docs && make html && cd ..")

    if os.path.exists("dist"):
        shutil.rmtree("dist")

    ctx.run("python3 -m build")
    ctx.run("twine check dist/*")

    if test:
        repo_flag = "--repository-url https://test.pypi.org/legacy/ "
    else:
        repo_flag = ""

    ctx.run(f"twine upload {repo_flag} dist/*")

    missing_tag = ctx.run(
        f"git ls-remote --exit-code origin refs/tags/{version}", warn=True
    )
    if not test and missing_tag.exited != 0:
        ctx.run(f"git push origin {version}")  # push tag


def run_and_check(ctx, command):
    result = ctx.run(command, warn=True, pty=True)
    if result.exited != 0:
        raise Exception(
            f"Command '{command}' failed with exit code: {result.exited}."
            f"Do you have Octopus in PATH?"
        )
    return result


@invoke.task
def generatePytestData(ctx):
    """
    Run octopus on the example folders to get the data needed for pytest.

    For the task you will need a supported version of octopus in your PATH.
    Octopus@13.0 supported

    In the end we trigger duplicate_with_missing_to_copytree which creates example
    folders which are missing one or many files. These are needed for the unit tests.

    pty=True as a transfer parameter is indispensable for macOS users. Since the ctx
    command is not executed without pty=True.On Linux computers ctx runs with and
    without pty=True.

    Parameters
    ----------
    ctx
        invoke context

    Returns
    -------
    None

    """
    print(
        "Generating test data.\n"
        "Make sure to have Octopus in your PATH. Otherwise,"
        " it will fail very quickly. \n"
        "If everything works, you will see a success message at the end."
    )

    def run_octopus(folder, mode):
        """
        Run octopus and save the log.
        Parameters
        ----------
        folder
            folder to run octopus in
        mode
            mode to run octopus in
        Returns
        -------
        str
            Command to be executed
        """
        return f"cd {folder} && octopus > out_{mode}.log 2>&1"

    def run_octopus_change_inp(folder, mode):
        """
        Change the inp file for the given folder to the given mode and run octopus .

        It also saves the log.

        Parameters
        ----------
        folder
            folder to run octopus in
        mode
            mode to run octopus in
        Returns
        -------
        str
            Command to be executed
        """
        return (
            f"cd {folder} && mv inp inp_gs && mv inp_{mode} inp &&"
            f" octopus > out_{mode}.log 2>&1 &&"
            f" mv inp inp_{mode} && mv inp_gs inp"
        )

    def run_gs(folder):
        return run_octopus(folder, "gs")

    def run_td(folder):
        return run_octopus_change_inp(folder, "td")

    def run_unocc(folder):
        return run_octopus_change_inp(folder, "unocc")

    with ctx.cd("tests/data/"):
        # gs
        run_and_check(ctx, run_gs("benzene"))
        run_and_check(ctx, run_gs("methane"))
        run_and_check(ctx, run_gs("2-h-BN_monolayer"))
        run_and_check(ctx, run_gs("3-bulk_Si"))
        run_and_check(ctx, "cd nested_runs && python3 create_runs.py")

        # td
        run_and_check(ctx, run_td("methane"))
        run_and_check(ctx, run_td("3-bulk_Si"))

        # only td inp file
        run_and_check(ctx, run_octopus("celestial_bodies", "td"))
        run_and_check(ctx, run_octopus("interference", "td"))

        # unocc
        # The gs files in static will not be overwritten, the new ones get added
        run_and_check(ctx, run_unocc("2-h-BN_monolayer"))

        # Copytree examples
        run_and_check(ctx, "python3 duplicate_with_missing_to_copytree.py")
    print("Generation of test data succeeded!")
