import os
from pathlib import Path

from postopus import Run


def test_run_instantiation():
    """
    Test checks if the default value for the path of a run object
    is the current working directory.
    """
    os.chdir(Path(__file__).parent / "data")
    run1 = Run("methane")
    run1_path = Path(os.path.join(os.getcwd(), run1.path))

    os.chdir("methane")
    run2 = Run()
    run2_path = Path(os.path.join(os.getcwd(), run2.path))

    assert run1_path == run2_path
