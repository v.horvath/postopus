from pathlib import Path

from postopus.datacontainers.util.output_collector import OutputCollector

methane_path = Path(__file__).parents[2] / "data" / "methane"
interference_path = Path(__file__).parents[2] / "data" / "interference"


def test_fields():
    check_fields_maxwell = {
        "td": [
            "maxwell_energy_density",
            "b_field-z",
            "b_field-x",
            "e_field_trans-y",
            "e_field-y",
            "e_field_trans-z",
            "e_field-z",
            "e_field_trans-x",
            "e_field-x",
            "b_field-y",
        ]
    }
    check_extensions_maxwell = {
        "td": ["x=0", "x=0,y=0", "x=0,z=0", "y=0", "y=0,z=0", "z=0"]
    }
    check_avail_systems = ["Maxwell", "default"]
    oc = OutputCollector(interference_path)
    assert oc._filesystem_systems == check_avail_systems
    maxwell_td_fields = oc.find_fields_and_extensions("Maxwell")
    assert maxwell_td_fields[0].keys() == check_fields_maxwell.keys()
    assert maxwell_td_fields[0]["td"].sort() == check_fields_maxwell["td"].sort()

    # check extensions as well
    assert maxwell_td_fields[1].keys() == check_extensions_maxwell.keys()
    assert maxwell_td_fields[1]["td"].sort() == check_extensions_maxwell["td"].sort()

    default_fields_and_extensions = oc.find_fields_and_extensions("default")
    assert default_fields_and_extensions[0]["td"] == []
    assert default_fields_and_extensions[1]["td"] == []


def test_fields_single_system():
    # Data for checking correctness
    check_fields_default = {
        "td": ["current-z", "current", "current-y", "current-x", "density"],
        "scf": ["density"],
    }
    check_extensions = {
        "td": ["ncdf", "vtk", "xsf", "z=0"],
        "scf": ["ncdf", "vtk", "xsf", "z=0"],
    }
    check_avail_systems = ["default"]

    oc = OutputCollector(methane_path)
    assert oc._filesystem_systems == check_avail_systems
    default_fields = oc.find_fields_and_extensions("default")
    # sort fields, so that we can ignore positions in the lists
    assert default_fields[0].keys() == check_fields_default.keys()
    assert default_fields[0]["td"].sort() == check_fields_default["td"].sort()
    assert default_fields[0]["scf"].sort() == check_fields_default["scf"].sort()

    # check extensions as well
    assert default_fields[1].keys() == check_extensions.keys()
    assert default_fields[1]["td"].sort() == check_extensions["td"].sort()
    assert default_fields[1]["scf"].sort() == check_extensions["scf"].sort()
