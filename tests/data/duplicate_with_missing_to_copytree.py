#!/usr/bin/env python
from __future__ import annotations

import os
import warnings
from itertools import product
from pathlib import Path
from shutil import copytree


def duplicate_with_missing_to_copytree(base: str, target: str, missing: list[Path]):
    """
    Creates a new "example" in Postopus' tests/data directory from an existing one.
    New example may have missing files/folders. Data is not copied, but hard linked.

    Parameters
    ----------
    base: str
        Name of the example that shall be "duplicated"
    target: str
        Name of the "duplicated" example (result folder)
    missing: list[str]
        List of files or folders in the original data that are not duplicated. Paths
        are relative to `base`.

        Examples
        --------
        Duplicate "methane" with missing "scf.0003" and "inp".

        >>> from pathlib import Path
        >>> duplicate_with_missing_to_copytree("methane", "methane_copy",
         ["output_iter/scf.0003", "inp"])
    """

    missing_paths = [Path(base) / elem for elem in missing]

    def ignore_patterns(src, names):
        check_paths = set(Path(src) / name for name in names)
        ignored_paths = check_paths.intersection(missing_paths)
        return set(ignored.name for ignored in ignored_paths)

    testdatadir = Path(__file__).parent

    # Check if there exists some example to duplicate
    basepath = testdatadir / base
    if not basepath.is_dir():
        raise FileNotFoundError(f"No folder '{base}' in path {testdatadir}!")

    # Check to not override stuff
    targetpath = testdatadir / target
    if targetpath.exists():
        warnings.warn(
            f"A file/folder named '{target}' already exists in {testdatadir}! Skipping"
        )
        return

    copytree(
        base,
        targetpath,
        ignore=ignore_patterns,
        copy_function=os.link,
    )


# Create extra test cases
duplicate_with_missing_to_copytree("methane", "methane_min_no_static", [Path("static")])
duplicate_with_missing_to_copytree(
    "methane", "methane_missing_scf_iter", [Path("output_iter/scf.0004")]
)
duplicate_with_missing_to_copytree(
    "methane", "methane_missing_td_iter", [Path("output_iter/td.0000003")]
)
# Only leave ncdf files for its. 5, 7, 12, 1, for testing.
missing_td_exts = [
    Path(f"output_iter/td.00000{i}/density.{ext}")
    for i, ext in product(("05", "07", "12", "01"), ("xsf", "vtk", "z=0"))
]

duplicate_with_missing_to_copytree(
    "methane", "methane_missing_td_extensions", missing_td_exts
)

duplicate_with_missing_to_copytree(
    "methane",
    "methane_missing_vector_dimensions",
    [
        Path("output_iter/td.0000000/current-z.z=0"),
        Path("output_iter/td.0000000/current-z.ncdf"),
        Path("output_iter/td.0000000/current-z.xsf"),
        Path("output_iter/td.0000000/current.vtk"),
    ],
)
