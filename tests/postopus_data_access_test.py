from pathlib import Path

import pandas as pd
import pytest

from postopus import Run

testdata_dir = Path(__file__).parent / "data"


def test_read_scalarfield():
    run = Run(testdata_dir / "methane")
    assert sorted(list(run.default.system_data.keys())) == sorted(["td", "scf"])


def test_read_vectorfield():
    run = Run(testdata_dir / "methane")
    assert sorted(list(run.default.system_data.keys())) == sorted(["td", "scf"])
    assert sorted(run.default.td.current.components) == ["x", "y", "z"]
    assert run.default.td.current.z.get(0, source="z=0").values.shape == (1, 27, 27)


def test_read_tdgeneralvectorfield():
    """
    Test reading td.general vector fields
    """
    run = Run(testdata_dir / "interference")
    assert list(run.Maxwell.system_data.keys()) == ["td"]
    assert sorted(run.Maxwell.td.total_e_field.dimensions) == ["x", "y", "z"]
    assert run.Maxwell.td.total_e_field.z.shape == (167, 2)
    assert isinstance(run.Maxwell.td.total_e_field.z, pd.DataFrame)
    assert run.Maxwell.td.total_e_field.z.attrs.keys() == {"units", "metadata"}
    assert run.Maxwell.td.total_e_field.z.attrs["units"] == {
        "Iter": "[Iter n.]",
        "t": "[hbar/H]",
        "E(1)": "[H/b]",
    }
    assert run.Maxwell.td.total_e_field.z.attrs["metadata"] == {
        "filename": "total_e_field_z",
        "dt": ["0.210656423428E-02 [hbar/H]"],
    }


def test_read_broken_vectorfield():
    with pytest.raises(
        FileNotFoundError,
        match=r"Error: inconsistent number of "
        r"files found for file_type [a-zA-z0-9!.:.,]",
    ):
        Run(testdata_dir / "methane_missing_vector_dimensions")


def test_bad_path():
    with pytest.raises(NotADirectoryError):
        Run(testdata_dir / "methane" / "inp")

    with pytest.raises(FileNotFoundError):
        Run(testdata_dir)


def test_bad_outputformat_param():
    # Check Error on not-present data-format
    with pytest.raises(ValueError):
        run = Run(testdata_dir / "methane")
        run.default.scf.density.get(1, source="y=0")

    # Check trying an illegal outputformat argument
    with pytest.raises(ValueError):
        run.default.scf.density.get(1, source="bla")
